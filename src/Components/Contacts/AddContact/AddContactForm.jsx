import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from 'react-redux';
import { addContact } from '../../../Actions/apiActionTypes';

class AddContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      firstNameIsSet: false,
      lastNameIsSet: false,
      companyIsSet: false,
      firstName: '',
      lastName: '',
      companyName: '',
      position: '',
      comment: '',
    };
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleFirstNameChanged = this.handleFirstNameChanged.bind(this);
    this.handleLastNameChanged = this.handleLastNameChanged.bind(this);
    this.handleCompanyChanged = this.handleCompanyChanged.bind(this);
    this.handlePositionChanged = this.handlePositionChanged.bind(this);
    this.handleCommentChanged = this.handleCommentChanged.bind(this);
  }

  handleClickOpen() {
    this.setState({
      isOpen: true,
    });
  }

  handleCancel() {
    this.setState({
      isOpen: false,
      firstNameIsSet: false,
      lastNameIsSet: false,
      companyIsSet: false,
      firstName: '',
      lastName: '',
      companyName: '',
      position: '',
      comment: '',
    });
  }

  async handleSave() {
    const contact = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      companyName: this.state.companyName,
      position: this.state.position,
      comment: this.state.comment,
    };
    this.props.addContact(contact);
    this.handleCancel();
  }

  handleFirstNameChanged(event) {
    const { value } = event.target;
    this.setState({ firstName: value });
    if (value.trim()) {
      this.setState({
        firstNameIsSet: true,
      });
    } else {
      this.setState({
        firstNameIsSet: false,
      });
    }
  }

  handleLastNameChanged(event) {
    const { value } = event.target;
    this.setState({ lastName: value });
    if (value.trim()) {
      this.setState({
        lastNameIsSet: true,
      });
    } else {
      this.setState({
        lastNameIsSet: false,
      });
    }
  }

  handleCompanyChanged(event) {
    const { value } = event.target;
    this.setState({ companyName: value });
    if (value.trim()) {
      this.setState({
        companyIsSet: true,
      });
    } else {
      this.setState({
        companyIsSet: false,
      });
    }
  }

  handlePositionChanged(event) {
    this.setState({
      position: event.target.value,
    });
  }

  handleCommentChanged(event) {
    this.setState({
      comment: event.target.value,
    });
  }

  render() {
    const {
      isOpen, firstNameIsSet, lastNameIsSet, companyIsSet,
    } = this.state;
    return (
      <div>
        <Button variant="contained" onClick={this.handleClickOpen}>
          Add new contact
        </Button>
        <Dialog open={isOpen} onClose={this.handleCancel} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Please fill in the required data</DialogTitle>
          <DialogContent>
            <TextField
              onChange={this.handleFirstNameChanged}
              autoFocus
              type="text"
              margin="dense"
              id="firstName"
              label="First name"
              value={this.state.firstName}
              fullWidth
              required
            />
            <TextField
              onChange={this.handleLastNameChanged}
              autoFocus
              type="text"
              margin="dense"
              id="lastName"
              label="Last name"
              value={this.state.lastName}
              fullWidth
              required
            />
            <TextField
              onChange={this.handleCompanyChanged}
              autoFocus
              type="text"
              margin="dense"
              id="companyName"
              label="Company name"
              value={this.state.companyName}
              fullWidth
              required
            />
            <TextField
              onChange={this.handlePositionChanged}
              autoFocus
              type="text"
              margin="dense"
              id="position"
              label="Position"
              value={this.state.position}
              fullWidth
            />
            <TextField
              onChange={this.handleCommentChanged}
              autoFocus
              type="text"
              margin="dense"
              id="comment"
              label="Comment"
              value={this.state.Comment}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCancel} color="primary">
              Cancel
            </Button>
            <Button
              onClick={this.handleSave}
              disabled={!lastNameIsSet || !firstNameIsSet || !companyIsSet}
              color="primary"
            >
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AddContactForm.propTypes = {
  addContact: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  addContact: (contact) => dispatch(addContact(contact))
});

export default connect(null, mapDispatchToProps)(AddContactForm);
