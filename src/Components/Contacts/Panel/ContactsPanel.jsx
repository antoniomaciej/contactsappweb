import React from 'react';
import PropTypes from 'prop-types';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import ContactDetails from '../Details/ContactDetails';
import { connect } from 'react-redux';
import { getRecentlyAddedContacts } from '../../../Actions/apiActionTypes';

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

class ContactsPanel extends React.Component {

  componentDidMount() {
    this.props.getRecentlyAddedContacts();
  }

  render() {
    const { error, isLoaded, items } = this.props;
    if (error) {
      return (
        <div>
          Error:
          {error.message}
        </div>
      );
    } else if (!isLoaded) {
      return (
        <Backdrop className="backdrop" open={!isLoaded}>
          <CircularProgress color="inherit" />
        </Backdrop>
      );
    } else if (items) {
      return (
        <div>
          <div>
            {items.map((contact, index) => (
              <ContactDetails key={index} contact={contact} />
            ))}
          </div>
        </div>
      );
    }

    return (<div>There are no contacts to display!</div>);
  }
}

ContactsPanel.propTypes = {
  classes: PropTypes.object.isRequired,
  getRecentlyAddedContacts: PropTypes.func.isRequired,
  error: PropTypes.any,
  isLoaded: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.object),
};

function mapStateToProps(state) {
  const invert = !state.contactsViewReducer.contactsHaveNotBeenLoaded;
  return {
    error: state.contactsViewReducer.error,
    isLoaded: invert,
    items: state.contactsViewReducer.recentlyAddedContacts,
  }

}

function mapDispatchToProps(dispatch) {
  return {
    getRecentlyAddedContacts: () => dispatch(getRecentlyAddedContacts()),
  }
}

const connectedPanel = connect(mapStateToProps, mapDispatchToProps)(ContactsPanel);

export default withStyles(useStyles)(connectedPanel);
