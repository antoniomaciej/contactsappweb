import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import contactsApiReducer from './Reducers/contactsApiReducer';
import contactsApiSaga from './Sagas/contactsApiSaga';
import contactsViewReducer from './Reducers/contactsViewReducer';

const sagaMiddleware = createSagaMiddleware();

const contactsAppReducer = combineReducers({
  contactsApiReducer,
  contactsViewReducer,
});

const store = createStore(
  contactsAppReducer,
  applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(contactsApiSaga);

export default store;
