import React from 'react';
import Bar from './Components/Navigation/Bar/Bar';
import './App.css';
import ContactsPanel from './Components/Contacts/Panel/ContactsPanel';

function App() {
  return (
    <div className="mainPanel">
      <Bar name="Contacts App" />
      <h1>Recently Added Contacts</h1>
      <ContactsPanel />
    </div>
  );
}

export default App;
