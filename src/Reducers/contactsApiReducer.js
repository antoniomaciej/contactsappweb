import { ADD_CONTACT, GET_RECENTLY_ADDED_CONTACTS } from '../Actions/apiActionTypes';

function contactsApiReducer(state = {}, action) {
  switch (action.type) {
    case ADD_CONTACT:
      return { ...state }
    case GET_RECENTLY_ADDED_CONTACTS:
      return { ...state }
    default:
      return state;
  }
}

export default contactsApiReducer;
