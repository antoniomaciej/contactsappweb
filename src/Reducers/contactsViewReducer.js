import {
  LOAD_RECENTLY_ADDED_CONTACTS,
  DISPLAY_ERROR_PARSING_JSON,
  TOGGLE_PANEL_BACKDROP
} from '../Actions/viewActionTypes';

const initialState = {
  recentlyAddedContacts: [],
  error: null,
  contactsHaveNotBeenLoaded: false,
};

export function contactsViewReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_RECENTLY_ADDED_CONTACTS:
      return { ...state, recentlyAddedContacts: action.recentlyAddedContacts };
    case DISPLAY_ERROR_PARSING_JSON:
      return { ...state, error: action.error };
    case TOGGLE_PANEL_BACKDROP:
      return { ...state, contactsHaveNotBeenLoaded: action.contactsHaveNotBeenLoaded };
    default:
      return state;
  }
}

export default contactsViewReducer;