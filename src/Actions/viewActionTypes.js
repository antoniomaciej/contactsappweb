export const LOAD_RECENTLY_ADDED_CONTACTS = 'LOAD_RECENTLY_ADDED_CONTACTS';
export const DISPLAY_ERROR_PARSING_JSON = 'DISPLAY_ERROR_PARSING_JSON';
export const TOGGLE_PANEL_BACKDROP = 'TOGGLE_PANEL_BACKDROP';

export function togglePanelBackDrop(contactsHaveNotBeenLoaded) {
  return ({
    type: TOGGLE_PANEL_BACKDROP,
    contactsHaveNotBeenLoaded,
  });
}

export function loadRecentlyAddedContacts(recentlyAddedContacts) {
  return ({
    type: LOAD_RECENTLY_ADDED_CONTACTS,
    recentlyAddedContacts,
  });
}

export function displayErrorParsingJson(error) {
  return ({
    type: DISPLAY_ERROR_PARSING_JSON,
    error,
  });
}
