export const ADD_CONTACT = 'ADD_CONTACT';
export const GET_RECENTLY_ADDED_CONTACTS = 'GET_RECENTLY_ADDED_CONTACTS';

export function addContact(contact) {
  return ({
    type: ADD_CONTACT,
    contact,
  });
}

export function getRecentlyAddedContacts() {
  return ({
    type: GET_RECENTLY_ADDED_CONTACTS,
  });
}
