import {
  all,
  apply,
  takeEvery,
  call,
  put,
} from 'redux-saga/effects';
import {
  ADD_CONTACT,
  GET_RECENTLY_ADDED_CONTACTS
} from '../Actions/apiActionTypes';
import {
  LOAD_RECENTLY_ADDED_CONTACTS,
  DISPLAY_ERROR_PARSING_JSON,
  TOGGLE_PANEL_BACKDROP
} from '../Actions/viewActionTypes';

export function* postContactAsync(action) {
  yield call(
    fetch,
    '/Contacts/Create',
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(action.contact),
    },
  );
  yield put({ type: GET_RECENTLY_ADDED_CONTACTS });
}

export function* getRecentlyAddedContacts() {
  yield put({ type: TOGGLE_PANEL_BACKDROP, contactsHaveNotBeenLoaded: true });
  const response = yield call(
    fetch,
    'contacts/RecentlyAddedContacts'
  );
  try {
    const contactsAsJson = yield apply(response, 'json', []);
    yield put({ type: LOAD_RECENTLY_ADDED_CONTACTS, recentlyAddedContacts: contactsAsJson });
  } catch (error) {
    yield put({ type: DISPLAY_ERROR_PARSING_JSON, error: error });
  } finally {
    yield put({ type: TOGGLE_PANEL_BACKDROP, contactsHaveNotBeenLoaded: false });
  }
}

export function* watchSaveContactAsync() {
  yield takeEvery(ADD_CONTACT, postContactAsync);
}

export function* watchGetRecentlyAddedContacts() {
  yield takeEvery(GET_RECENTLY_ADDED_CONTACTS, getRecentlyAddedContacts);
}

export default function* contactsApiSaga() {
  yield all([
    watchSaveContactAsync(),
    watchGetRecentlyAddedContacts(),
  ]);
}
